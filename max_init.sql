drop DATABASE if exists para;

CREATE DATABASE para;

use para;

CREATE table cas (
    id int auto_increment primary key,
    title varchar(255) UNIQUE,
    type varchar(255),
    location varchar(255),
    date varchar(255),
    comments text,
    month int,
    weather boolean
);

CREATE TABLE category (
    id int auto_increment primary key,
    parent_id int references category.id,
    name varchar(255),
    cat_type enum("geo", "report")
);

CREATE TABLE cas_cat(
    cas_id int references cas.id,
    cat_id int references category.id
);