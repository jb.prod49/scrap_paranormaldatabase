DROP DATABASE IF EXISTS paranormal;
CREATE DATABASE paranormal;
USE paranormal;

CREATE TABLE `case` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `location_id` int,
  `place_id` int,
  `title` varchar(50),
  `type` varchar(50),
  `date` varchar(50),
  `comment` text,
  `month` int
);

CREATE TABLE `people` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` varchar(50)
);

CREATE TABLE `case_people` (
  `people_id` int,
  `case_id` int
);

CREATE TABLE `place` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` varchar(50)
);

CREATE TABLE `location` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `parent_id` int,
  `name` varchar(50)
);

ALTER TABLE `case` ADD FOREIGN KEY (`location_id`) REFERENCES `location` (`id`);

ALTER TABLE `case` ADD FOREIGN KEY (`place_id`) REFERENCES `place` (`id`);

ALTER TABLE `case_people` ADD FOREIGN KEY (`case_id`) REFERENCES `case` (`id`);

ALTER TABLE `case_people` ADD FOREIGN KEY (`people_id`) REFERENCES `people` (`id`);

ALTER TABLE `location` ADD FOREIGN KEY (`parent_id`) REFERENCES `location` (`id`);
