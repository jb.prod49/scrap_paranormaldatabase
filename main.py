from urllib.request import urlopen
from bs4 import BeautifulSoup
import mysql.connector
from mysql.connector import Error
from urllib.request import urlopen
from config.bdd_config import DB_CONFIG,DB_CONFIG2

#---------------------------------------#
#------ déclaration des fonctions ------#
#---------------------------------------#    
def connect_bdd(db_config):
    connect = mysql.connector.connect(**db_config)
    return connect

def close_db_connection(conn: mysql.connector.connect):
    if conn.is_connected():
        conn.close()  

from mysql.connector import Error

def execute_query(query: str, params=None, connect=None, fetch=True):
    try:
        cursor = connect.cursor()
        # print(f"Executing query: {query} with params: {params}")
        cursor.execute(query, params)
        
        if fetch:
            result = cursor.fetchall()
        else:
            connect.commit()  
            result = None
        
        return result
    except Error as e:
        print(f"Erreur lors de l'exécution de la requête : {e}")
        return None
    finally:
        cursor.close()  


def get_page_content(url):
    print("https://www.paranormaldatabase.com"+url)
    return urlopen("https://www.paranormaldatabase.com"+url).read()

def ajout_category(titre_cat,conn):
    query = """
                SELECT
                    id
                FROM
                    category
                WHERE
                    name LIKE %s
    """
    verif_cat = execute_query(query, (titre_cat,),conn)
    if verif_cat is not None and verif_cat != []:
        id_cat = verif_cat[0][0]
        pass
    else:
        query = """
                    INSERT INTO category (name) VALUES (%s);
                """
        execute_query(query, (titre_cat,),conn, fetch=False)
        
        query = """
            SELECT LAST_INSERT_ID() FROM category
        """
        id_cat = execute_query(query, (),conn)[0][0]
    return id_cat

def ajout_sous_category(id_cat,titre_ss_cat,conn):
    query = """
                SELECT
                    id
                FROM
                    category
                WHERE
                    name LIKE %s
    """
    verif_ss_cat = execute_query(query, (titre_ss_cat,),conn)
    if verif_ss_cat is not None and verif_ss_cat != []:
        id_ss_cat = verif_ss_cat[0][0]
        pass
    else:
        query = """
                    INSERT INTO category (parent_id,name) VALUES (%s,%s);
                """
        execute_query(query, (id_cat,titre_ss_cat),conn, fetch=False)
        
        query = """
            SELECT LAST_INSERT_ID() FROM category
        """
        id_ss_cat = execute_query(query, (),conn)[0][0]
    return id_ss_cat

#-------------------------------------------------#
#-------- exécution du fichier init.sql ----------#
#-------------------------------------------------#
conn = connect_bdd(DB_CONFIG)
init_sql = 'max_init.sql'

with open(init_sql, 'r') as file:
    sql_script = file.read()

sql_statements = sql_script.split(';')

for statement in sql_statements:
     if statement.strip():
        cursor = conn.cursor()
        try:
            cursor.execute(statement)
            conn.commit()
        # except mysql.connector.Error as err:
            # print(f"Une erreur est survenue: {err}")
        finally:
            cursor.close()
            
#-----------------------------------#
#------ récuperer les données ------#
#-----------------------------------#
# conn = connect_bdd(DB_CONFIG2)
count_loop = 0
count_new_cas = 0

# get content home
html_home = get_page_content("/index.html")

# get category home
list_cat_home = []

soup = BeautifulSoup(html_home, features="html.parser")

liste_thirds = soup.find_all(class_="w3-third")
for third in liste_thirds:
    lien = third.find('a')
    url= lien.get('href')
    list_cat_home.append(url)

liste_halfs = soup.find_all(class_="w3-half")
for third in liste_halfs:
    lien = third.find('a')
    url= lien.get('href')
    list_cat_home.append(url)

liste_q = soup.find_all(class_="w3-quarter")
for third in liste_q:
    lien = third.find('a')
    url= lien.get('href')
    list_cat_home.append(url)

for cat in list_cat_home:
    html_cat = get_page_content(cat)
    soup = BeautifulSoup(html_cat, features="html.parser")
    titre_cat = soup.select_one(".w3-panel > h4:nth-child(1)")
    titre_cat.find("a").extract()
    titre_cat = titre_cat.getText()
    titre_cat = titre_cat.replace("> ", "")
    titre_cat = titre_cat.strip()
    
    #insert catégorie
    id_cat = ajout_category(titre_cat, conn)
    
    list_sous_cat = []

    liste_thirds = soup.find_all(class_="w3-third")
    for third in liste_thirds:
        lien = third.find('a')
        url= lien.get('href')
        list_sous_cat.append(url)

    liste_halfs = soup.find_all(class_="w3-half")
    for third in liste_halfs:
        lien = third.find('a')
        url= lien.get('href')
        list_sous_cat.append(url)

    for ss_cat in list_sous_cat:
        html_sous_cat = get_page_content(ss_cat)
        soup = BeautifulSoup(html_sous_cat, features="html.parser")
        # prendre plutot le titre h3 de la page des cas
        titre_ss_cat = soup.select_one(".w3-panel > h4:nth-child(1)")
        titre_ss_cat.find("a").extract()
        titre_ss_cat.find("a").extract()
        titre_ss_cat = titre_ss_cat.getText()
        titre_ss_cat = titre_ss_cat.replace("> ", "")
        titre_ss_cat = titre_ss_cat.strip()
        
        #ajout de la sous catégorie
        id_ss_cat = ajout_sous_category(id_cat,titre_ss_cat,conn)

        # refaire tout pour chaque page de la pagination
        qs = "?"
        while(qs):
            html_sous_cat = get_page_content(ss_cat+qs)
            soup = BeautifulSoup(html_sous_cat, features="html.parser")

            list_cas = soup.select("div.w3-panel:nth-child(4) > .w3-half > .w3-border-left.w3-border-top.w3-left-align > p")

            for cas in list_cas:
                # parser le cas

                # title varchar(255),
                titre_cas = cas.select_one("h4").get_text()

                # recup le fameux P
                p = cas.select_one("p:nth-last-child(1)")
                p_text = p.get_text().split("\n")
                # location varchar(255),
                p_text[0] = p_text[0].replace("Location: ", "")
                # type varchar(255),
                p_text[1] = p_text[1].replace("Type: ", "")
                # date varchar(255),
                p_text[2] = p_text[2].replace("Date / Time: ", "")
                # comments varchar(255),
                p_text[3] = p_text[3].replace("Further Comments: ", "")
                
                if titre_cat == "Calendar":
                    pass
                    # month dans la cat calendar
                    # weather dans la cat calencar


                conn = connect_bdd(DB_CONFIG2)
                cursor = conn.cursor()
                query = """
                            SELECT
                               title
                            FROM
                                cas
                            WHERE
                                title LIKE %s
                        """
                verif = execute_query(query, (titre_cas,),conn, fetch=True)

                if verif is not None and verif != []:
                    pass
                else:
                    query = """
                            INSERT INTO cas (title, type, location, date, comments) VALUES (%s, %s, %s, %s, %s);
                    """
                    execute_query(query, (titre_cas, p_text[1], p_text[0], p_text[2], p_text[3]),conn, fetch=False)
                    query = """
                                SELECT LAST_INSERT_ID() FROM category
                            """
                    id_cas = execute_query(query, (),conn)[0][0]
                    query = """
                            INSERT INTO cas_cat (cas_id, cat_id) VALUES (%s, %s);
                    """
                    execute_query(query, (id_cas,id_ss_cat),conn, fetch=False)
                    
                    count_new_cas +=1
                    print("nombre de nouveau cas: ", count_new_cas)
                # next
                nav_btn = soup.select(".w3-quarter.w3-container h5")
                qs = None
                for btn in nav_btn:
                    if btn.get_text().find('next') > 0:
                        qs = btn.select_one("a").get("href")
                count_loop += 1
                print( "nombre de tour :",count_loop)

