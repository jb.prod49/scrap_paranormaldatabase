import time
import requests
from random import *
import mysql.connector
from mysql.connector import Error
from urllib.request import urlopen
from bs4 import BeautifulSoup as bs
from config.bdd_config import DB_CONFIG,DB_CONFIG1

#---------------------------------------#
#------ déclaration des fonctions ------#
#---------------------------------------#    
def connect_bdd(db_config):
    connect = mysql.connector.connect(**db_config)
    return connect

def close_db_connection(conn: mysql.connector.connect):
    if conn.is_connected():
        conn.close()  

def execute_query(query: str, params=None, connect=None, fetch=True):
    try:
        cursor = connect.cursor()
        # print(f"Executing query: {query} with params: {params}")
        cursor.execute(query, params)
        result = cursor.fetchall() if fetch else None
        connect.commit()
        return result
    except Error as e:
        # print(f"Erreur lors de l'exécution de la requête : {e}")
        return None
    
def filtre_data(data:list)->list:
    filtered_data = [item for item in data if item.strip()]
    return filtered_data

def format_p(text:str)->list:
    lines = text.split('\n')
    result = {}

def take_html(url):
    response = requests.get(url)
    html = response.content
    soup = bs(html, "lxml")
    return soup

def take_title_h3(soup)->str:
    title_h3 = soup.h3.get_text()
    return title_h3

def take_div(soup)->list:
    selector = ".w3-half > div.w3-border-left, .w3-half > div.w3-border-top, .w3-half > div.w3-left-align"
    list_div = soup.select(selector)
    
    # list_div =  soup.findAll("div",class_=(["w3-border-left", "w3-border-top","w3-left-align"]))
    new_list_div = []
    for div in list_div:
        if div is not None:
            new_list_div.append(div)
    return new_list_div

def take_title_h4(soup)->list:
    list_h4 = soup.findAll("h4")
    new_list_h4 = []
    for h4 in list_h4:
        if h4 is not None:
            new_list_h4.append(h4.get_text())
    return new_list_h4

def take_p(soup):
    list_p = soup.find_all("p")
    temp_list_p = []
    for p in list_p:
        text = p.get_text()
        if '\n' in text:
            lines = text.split('\n')
            temp_list_p.extend(lines)
        else:
            temp_list_p.append(text)
    result = filtre_data(temp_list_p) 
    return result

def format_data(line):
    temp = {}
    result = []
    for x in line:
        if ':' in x:
            key, value = x.split(':', 1)
            key = key.strip().lower().replace(' ', '_').replace('/', '')
            value = value.strip()
            temp = {key: value}
            result.append(temp)
    return result

def take_url(soup):
    list_url = soup.find_all("a")
    return list_url

def black_list():
    black_list = ["https://mastodonapp.uk/@paranormaldatabase",
                "#myHeader",
                "/search.htm",
                "legal/index.html",
                "/ssl/contact001.html",
                "/calendar/Pages/calendar.html",
                "/about/index.html",
                "/links/links.htm",
                "/socialmedia/index.html"
                ]
    return black_list

def take_next(soup):
    selector= "div.w3-quarter:nth-child(3) > h5:nth-child(1) > a:nth-child(1)"
    next = soup.select(selector)
    return next

def tak_all_url(initial_url):
    table_url = [initial_url]
    black_list = black_list()
    checked_urls = set()
    
    i = 0
    for current_url in table_url:
        random = randint(1,5)
        time.sleep(random)
                
        if current_url not in black_list:
            if current_url not in checked_urls:
                checked_urls.add(current_url)
                print("current url :", current_url)
                full_url = "https://www.paranormaldatabase.com" + current_url
                soup = take_html(full_url)
                time.sleep(randint(1,5))
                url_page = take_url(soup)
                
                for link in url_page:
                    href = link.get('href')
                    print("url taked:", href)
                    if href and href not in black_list:
                        if href not in table_url:
                            table_url.append(href)
                            print("url append")
                        else:
                            print("url in table")
                    else:
                        print("url in black list")
            else:
                print("already visited")
        else:
            print("url in black list")
        i += 1
        print("---- ", i, "/", len(table_url), " ----")
    return table_url

#-------------------------------------------------#
#-------- exécution du fichier init.sql ----------#
#-------------------------------------------------#
conn = connect_bdd(DB_CONFIG)
init_sql = 'init.sql'

with open(init_sql, 'r') as file:
    sql_script = file.read()

sql_statements = sql_script.split(';')

for statement in sql_statements:
     if statement.strip():
        cursor = conn.cursor()
        try:
            cursor.execute(statement)
            conn.commit()
        except mysql.connector.Error as err:
            print(f"Une erreur est survenue: {err}")
        finally:
            cursor.close()
            
#-------------------------------#
#------ récuperer les URL ------#
#-------------------------------#
url = "/index.html"
list_all_url= tak_all_url(url)
    

#----------------------------------------------#
#------ récuperer les données d'une page ------#
#----------------------------------------------#
url = "https://www.paranormaldatabase.com/berkshire/berkdata.php"
soup = take_html(url)

title_h3 = take_title_h3(soup)
list_div = take_div(soup)
tuple_datas = {}
datas = []
for div in list_div:
    if div is not None:
        title_h4 = take_title_h4(div)
        p = take_p(div)
        lignes = format_data(p)
        if title_h4 and lignes:
            tuple_datas = {"title": title_h4, "élements": lignes}
            datas.append(tuple_datas)
for data in datas: 
    print(data)
